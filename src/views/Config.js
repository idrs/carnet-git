import m from "mithril";
import Carnet from "../model/Carnet";

configList = [
    {
        label:"Dépot",
        variable: "depot",
    },
    {
        label:"Dossier de travail",
        variable: "workingDir",
    },
    {
        label:"Username",
        variable: "username",
    },
    {
        label:"Password",
        variable: "password",
        type: "password"
    },
    {
        label:"Author",
        variable: "author",
    },
    {
        label:"Email",
        variable: "email",
        type: "email",
    },
]

function Config() {
    //console.log("status: ",Carnet.status)
    //console.log("menu: ",Carnet.menu)
    console.log("config depot",Carnet.depot);
    return {
        view: function (vnode) {
            //console.log('Barre')
            //console.log(vnode.attrs)
            
            return m("#config.section",m('#inputs',
            m('ul',configList.map((c)=>m("li",
                m(".labels",c.label),
                m('input',
                    {
                        oninput:function(e){
                            Carnet[c.variable]=e.target.value},
                        value: Carnet[c.variable],
                        placeholder: c.label,
                        type: (c.type?c.type:"text"),
                    })
                ))
            ),
            m('#buttons',
            m('button',{onclick:()=>{
                console.log("enregistrement");
                Carnet.saveSettings();
            }},"Enregistrer"),
            m('button',{onclick:()=>{
                console.log("click");
                console.log("Depot: ",Carnet.depot);
                Carnet.loadGit()
            }},"Clone"),
            m('button',{onclick:()=>{
                console.log("cleaning");
                Carnet.clean();
            }},"Wipe"))
        ),)

            
                
        }
    }
}



export default Config;
