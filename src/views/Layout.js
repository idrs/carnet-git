var m = require( "mithril");
var Barre = require("./Barre");
var Carnet = require("../model/Carnet")
var Menu = require("./Menu")

function Layout() {
    return {
    view: function(vnode) {
        console.log('vnode of layout',vnode)
        return m("#layout.columns",
            //m(Barre),
                m("nav.filer.is-hidden-touch.column.is-narrow",m(Menu)),
            m(".main.column.mt-3", Carnet.loading?"Chargement...":vnode.children),

        )
        
    }
}
}

module.exports = Layout