import m from "mithril";
import Carnet from '../model/Carnet';


function depth(name){
    return (name.split('/').length-3);
}

array2m = function(array){
    
    return array.sort().map((n)=>m(".name",{onclick:()=>m.route.set("/dir/:path...",{path:n})},"-".repeat(depth(n))+n.split("/").pop()))

}

function Menu() {

    return {
        view: function(){
            return m('.menu.p-2',{style:"border:solid 1px black"},
                m('a.menu-label',{onclick:()=>m.route.set('/config')},"Configuration"),
                m("p#menuTitre.menu-label",{onclick:()=>m.route.set('/accueil')},"Structure"),
                array2m(Object.keys(Carnet.dirList))
                )
        }
    }
}

module.exports = Menu
