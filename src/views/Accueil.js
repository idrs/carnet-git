import m from "mithril";
import Carnet from '../model/Carnet';

function byName(a,b) {
    return (a.name < b.name);
}

function Accueil() {
    return {
        view: function(vnode){
            
            console.log('dirList dir: ',Carnet.dirList)
            console.log('fileList: ',Carnet.fileList)
            return m('#accueil.columns',m('ol.column', 
                Object.values(Carnet.dirList).map((d)=>m("li",{onclick:()=>m.route.set('/dir/'+d.path)},d.path))),
                    m("ol.column",Object.values(Carnet.fileList).map((f)=>m("li",{onclick:()=>m.route.set('/edit/'+f.filepath.slice(1)+'/'+f.filename)},f.filename)))
            );
        }
    }
}

export default Accueil
