import m, { route } from "mithril";
import Carnet from "../model/Carnet";



function Barre() {
    var menu=false;
    var modal = false;
    var tail = [];
    function toggleMenu(){
        menu = !menu;
    }
    //Object.keys(Carnet).forEach((k)=>console.log(k+" type: "+typeof(Carnet[k])))
    function logModal(){
        console.log("modal launched")
        modal = !modal;
        if(modal) {
            tail = Carnet.status.slice(-10);
        }
        else {
            tail = [];
        } 
    }
    return {
        view: function (vnode) {
            //console.log('Barre')
            //console.log(Carnet.status)
            
            return m("nav.navbar#barre", {style:"border-bottom: black solid 1px"},
                m("div#barreTitre.navbar-brand", 
                    m(".navbar-item",{ onclick: (e) => route.set("/accueil") },
                    m(".barreTitre", "CarnetGit")),
                    m('.navbar-item#git-anim'+(Carnet.gitAnim?".is-active":""),{onclick:Carnet.pull},'<o>'),
                    m('.navbar-burger'+(menu?".is-active":""),
                        {onclick:()=>toggleMenu(),role:"button", "aria-label":"menu", "aria-expanded":"false", "data-target":"navbarElements"},
                        m("span",{"aria-hidden":"true"},""),
                        m("span",{"aria-hidden":"true"},""),
                        m("span",{"aria-hidden":"true"},""))),
                m("#navbarElements.navbar-menu"+(menu?".is-active":""),
                    m(".navbar-end",
                        m("a.#status.navbar-item",{onclick:()=>logModal()},Carnet.getstatus()),
                        m("a.#action.navbar-item",
                            { onclick: (e) => alert("Action !") }, "ICON"))
                ),
                m(".modal"+(modal?".is-active":""),
                    m(".modal-background",''),
                    m(".modal-content.content",m('.box',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        tail.map((p)=>m('p',p)))),
                    m("button.modal-close.is-large",{onclick:logModal,"aria-label":"close"},'')
                )

            
            )
                
        }
    }
}



export default Barre;
