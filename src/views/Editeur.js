import m from "mithril";
import Carnet from '../model/Carnet';


function Aside(){
    
    return {
        view: function(vnode){
            console.log("aside comp ",Carnet.note)
            console.log("filter ",Object.keys(Carnet.note)
            .filter((k)=> (k!=="content" && k!=="raw" && k.slice(0,1) !=='#' && k !== '')))
            return m("aside.column.p-4.is-hidden-touch.is-narrow",
                    Object.keys(Carnet.note)
                        .filter((k)=> (k!=='content' && k!=='raw' && k.slice(0,1) !=='#' && k !== ''))
                        .map((k)=> m(".block",
                            m('p',k),
                            m('textarea.textarea.is-small',{style: "min-height:1em;overflow:hidden;",oninput:(e)=>Carnet.note[k]=e.target.value},Carnet.note[k])
        
                        )
                        )
            
            );
        }
    }
}

function Editeur(vnode){
    Carnet.loadNote(decodeURI(vnode.attrs.path));
    return {
        view: function(vnode){
            
            console.log("note chargée: ",Carnet.note)
            return m("#editgrid.columns",m(".box#editor.column",
                m('textarea.textarea',{
                    oninput:(e)=>Carnet.note.content = e.target.value},Carnet.note.content
                ),
                m('button',{onclick:()=>Carnet.saveNote()},"Save/Commit")
            ),
            m(Aside)
            )
        }
    }
}

export default Editeur
