import m from "mithril";
import Carnet from '../model/Carnet';


function Aside(){
    
    return {
        view: function(vnode){
            console.log("aside comp ",Carnet.note)
            console.log("filter ",Object.keys(Carnet.note)
            .filter((k)=> (k!=="content" && k!=="raw" && k.slice(0,1) !=='#' && k !== '')))
            return m("aside.p-4",
                    Object.keys(Carnet.note)
                        .filter((k)=> (k!=='content' && k!=='raw' && k.slice(0,1) !=='#' && k !== ''))
                        .map((k)=> m(".block",
                            m('p',k),
                            m('textarea.textarea.is-small',{style: "min-height:1em;overflow:hidden;",oninput:(e)=>Carnet.note[k]=e.target.value},Carnet.note[k])
        
                        )
                        )
            
            );
        }
    }
}

export default Aside;