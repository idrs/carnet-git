import m from "mithril";
import Carnet from '../model/Carnet';

function byName(a,b) {
    return (a.name < b.name);
}

function Dirvue() {
    console.log("Dirvue accessed with vnode ",vnode);
    return {
        view: function(vnode){
            var content = [];
            var dir = "/"+vnode.attrs.path;
            if(Object.values(Carnet.fileList).length>0){
            console.log("reaching "+vnode.attrs.path)
            //console.log("watching Carnet.tree: ",JSON.stringify(Carnet.tree))
            console.log('fileList: ',JSON.stringify(Carnet.fileList))
            console.log("fileList values: ",Object.values(Carnet.fileList))
            var notes = Object.values(Carnet.fileList).filter((note)=>note.filepath===dir).sort(byName);
            console.log("notes in the room: ",notes);
            for(var k of Object.keys(Carnet.dirList)){
                console.log("k: ",k,"\n   val: ",Carnet.dirList[k]);
            }
            console.log('dirList dir: ',Carnet.dirList[dir])
            if(Carnet.dirList[dir].subdir){
                content[0]=Carnet.dirList[dir].subdir.map((d)=>
                        m('li',{onclick:()=>m.route.set('/dir/'+d.slice(1))},d.split('/').pop())
                    )
            }
            content.push(notes.map((f)=>
                        m('li',{onclick:()=>m.route.set('/edit/'+f.filepath.slice(1)+'/'+f.filename)},f.filename)
                    ));
            }
            console.log("content: ",content)
            return m('#dirview',m('ol', content
            )
            )
        }
    }
}

export default Dirvue
