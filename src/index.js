import m, { route } from "mithril";

import Carnet from "./model/Carnet";
import Layout from "./views/Layout"
import DirVue from "./views/Dirvue.js";
import Barre from "./views/Barre";
import Config from "./views/Config";
import Editeur from "./views/Editeur";
import Accueil from './views/Accueil';

import 'bulma/css/bulma.css'


route(document.getElementById("app"), "/accueil", {
    "/accueil": {
        render: function(vnode) {
            return [m(Barre,vnode.attrs),m(Layout,m(Accueil,vnode.attrs))]
        }
    },
    "/config": {
        render: function(vnode) {
            return [m(Barre,vnode.attrs),m(Layout,m(Config))]
        }
    },
    "/dir/:path...": {
        render: function(vnode){
            return [m(Barre,vnode.attrs),m(Layout,m(DirVue,vnode.attrs))]
        }
    },
    "/edit/:path...": {
        render: function(vnode){
            return [m(Barre,vnode.attrs),m(Layout,m(Editeur,vnode.attrs))]
        }
    },
    
});