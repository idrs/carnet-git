import m,{ redraw } from "mithril";

import LightningFS from '@isomorphic-git/lightning-fs';
//const path = require('path')
import { clone,pull } from 'isomorphic-git';
import http from 'isomorphic-git/http/node';

import stream from "mithril/stream";

var fs = new LightningFS('fs')
var pfs = fs.promises;

function getExt(str){
    let ext = str.match(/\.([^\.]+)/)
    if(ext){return ext[1]}
    else{return "unknown"}
}

function lookupSavedPassword (url) {
    //let auth = lookupSavedPassword(url)
    console.log(Carnet.username+" "+Carnet.password)
    if (Carnet.username && Carnet.password){
        return {username: Carnet.username, password: Carnet.password};
    }
    else{
    //JSON.parse(localStorage.getItem("carnet-auth"));
    
    if (confirm('This repo is password protected. Ready to enter a username & password?')) {
        Carnet.username = prompt('Enter username');
        Carnet.password = prompt('Enter password');
        Carnet.saveSettings();
        return {username: Carnet.username, password: Carnet.password};                    
    }  
    else {
      return { cancel: true }
    }
  }
}




var Carnet = {
    loading: true,
    dir: "/carnet",
    curDir: "/carnet",
    ls: [],
    notes: [],
    note: {},
    workingDir: "",
    status: [],
    username: "",
    password: "",
    depot: "",
    auth: null,
    author: "",
    email: "",
    menu: ".is-active",
    dirList: {},
    fileList: {},
    tree: {
        path:"",
        subdir:[],
        files:[],
        ext:"dir"
    },
    log: "",
    gitAnim: false,
    animToggle: function(){
        Carnet.gitAnim = !Carnet.gitAnim;
        redraw();
    },
    clean: function(){
        fs = new LightningFS('fs', { wipe: true });  
        redraw();
    },
    saveSettings: function(){
        var toSave = {}
        for(var prp of ["workingDir","username","password","depot","author","email"]){
            toSave[prp] = Carnet[prp];
        }
        localStorage.setItem('carnet-settings',JSON.stringify(toSave));
        Carnet.setstatus("saved settings to localStorage");
    },
    init: function(){
        Carnet.loading = true;
        var toLoad = JSON.parse(localStorage.getItem('carnet-settings'));
        if(toLoad){
            for(var k of Object.keys(toLoad)){
                Carnet[k]=toLoad[k];
            }
            pfs.readdir(Carnet.dir).then((e)=>{
                console.log('init');
                Carnet.setstatus( "Initialising !");
                Carnet.curDir = Carnet.dir+'/'+Carnet.workingDir;
                //Carnet.ls = e;
                Carnet.tree.name = Carnet.workingDir;
                Carnet.tree.path = Carnet.curDir;
                Carnet.populateTree(Carnet.dir+"/"+Carnet.workingDir,Carnet.tree);
            });
        }
        else{
            Carnet.setstatus("pas de paramètres en mémoire");
            //pfs.mkdir(Carnet.dir);
            
        }
        
    },
    setstatus: function(txt){
        //console.log("Set status...",txt)
        const t = new Date();
        Carnet.status.push(t.toLocaleTimeString('fr-FR')+' '+txt);
        //redraw();
    },
    getstatus: function(){ return Carnet.status[Carnet.status.length-1]},
    loadGit: function(){
        console.log("launch git with ", Carnet.depot)
        Carnet.setstatus("Cloning...");
        Carnet.gitAnim = true;
        pfs.mkdir(Carnet.dir).then(()=>

        clone({ 
            fs, 
            http,
            dir: Carnet.dir, 
            url: Carnet.depot,
            corsProxy: 'https://cors.isomorphic-git.org',
            onAuth: url => {
                let auth = lookupSavedPassword(url)
                if (auth) return auth
            
                if (confirm('This repo is password protected. Ready to enter a username & password?')) {
                  auth = {
                    username: prompt('Enter username'),
                    password: prompt('Enter password'),
                  }
                  return auth
                } else {
                  return { cancel: true }
                }
              }
        })
        .then((l)=> {
            console.log(JSON.stringify(l))
            Carnet.setstatus( "Cloning done !");
            Carnet.setstatus( "Populate structure tree...");
            Carnet.populateTree(Carnet.dir+"/"+Carnet.workingDir,Carnet.tree);
            //redraw();
        }))
    },
    pull: function(){
        Carnet.gitAnim = true;
        author = {
            name: Carnet.author,
            email: Carnet.email,
            timestamp: Math.floor(Date.now()/1000),
        };
        pull({
            fs,
            http,
            dir: '/carnet',
            singleBranch: true,
            author: author,
            onAuth:  lookupSavedPassword
          }).then((e)=>{
              Carnet.setstatus("Done pulling...");
              //Carnet.setstatus(e);
              Carnet.animToggle()
        });
          
    },
    //deprecated
    getDir: function(dir){
        console.log("getDir deprecated ",dir);
        pfs.readdir(dir).then((e)=>{
            Carnet.curDir = dir;
            Carnet.ls = e;
            console.log('in getDir:\n->dir splice split: ',dir.slice(1).split('/'))
            var spath = dir.slice(1).split('/').slice(2)
            
            Carnet.notes = Carnet.tree;
            spath.forEach((dir)=>{
                console.log(Carnet.notes,dir);
                Carnet.notes=Carnet.notes.subdir.find((sd)=>sd.name===dir)
            })
            Carnet.notes = Carnet.tree
            Carnet.setstatus( "Changing dir to "+dir);
            //Object.keys(Carnet).forEach((k)=>console.log(k+" type: "+typeof(Carnet[k])))
            //m.redraw.strategy("diff");
            redraw();
        });
    },
    getTo: function(path){
        console.log("getTo deprecated ",dir);
        pfs.stat(path).then((stats)=>{
            if(stats.isDirectory()){
                Carnet.getDir(path);
            }
            else{
                Carnet.setstatus("looking for "+path)
            }
        })
    },
    loadNote: function(path){
        Carnet.note = {};
        console.log("path of loadNote: ",path)
        pfs.readFile('/'+path,"utf8").then((file)=>{
            Carnet.note.raw = file;
            //console.log(file)
            //scanning yaml part
            if(file.trim().slice(0,3)==="---"){
                var endPart = file.indexOf("---",3);
                if(endPart>0){
                    Carnet.note.content = file.slice(endPart+3).trim();
                    file.slice(4,endPart).split('\n').forEach((l)=>{
                        var idxSep = l.indexOf(':');
                        var args = [l.slice(0,idxSep).trim(),l.slice(idxSep+1).trim()];
                        if(args[0] in ["tags","category","keywords"]){
                            Carnet.note[args[0]]=args[1].split(/\s*[,|\[|\]]\s*/).filter((s)=>s!=="");
                        }
                        else{
                            //console.log(args)
                            Carnet.note[args[0]]=args[1].trim();
                        }
                    })

                }
                
            }
            else{
                Carnet.note.content = file;
            }
            m.redraw();
        })
    },
    saveNote: function(){
        
    },
    populateTree: function(root,tree,incall){ //root must be a directory, do i need to check ?
        if(!incall){
            Carnet.setstatus("Populating tree")
            incall = 0
        }
        pfs.readdir(root).then((ls)=> {
            console.log(" ".repeat(2*incall)+root+": "+JSON.stringify(ls));
            ls.forEach((e)=>{
                pfs.stat(root+'/'+e).then((stats)=>{
                        if(stats.isDirectory()){
                            //Carnet.setstatus("adding to dirs "+e);
                            var sub = {name:e,subdir:[],files:[],ext:"dir",path:root+'/'+e};
                            //Carnet.tree.push({path:e,ext:"dir"});
                            Carnet.dirList[sub.path]=sub;
                            tree.subdir.push(sub.path);
                            Carnet.populateTree(root+'/'+e,sub,incall+1);
                            
                        }
                        else{
                            //Carnet.setstatus("adding to files "+e);
                            //tree.files.push({name:e,ext:getExt(e)});
                            var newnote = {filename:e,ext:getExt(e),filepath:root};
                            if(newnote.ext==="md"){
                                pfs.readFile(root+'/'+e,"utf8").then((file)=>{
                                    newnote.raw = file;
                                    //console.log(file)
                                    //scanning yaml part
                                    if(file.trim().slice(0,3)==="---"){
                                        var endPart = file.indexOf("---",3);
                                        if(endPart>0){
                                            newnote.content = file.slice(endPart+3).trim();
                                            file.slice(4,endPart).split('\n').forEach((l)=>{
                                                var idxSep = l.indexOf(':');
                                                var args = [l.slice(0,idxSep).trim(),l.slice(idxSep+1).trim()];
                                                if(args[0] in ["tags","category","keywords"]){
                                                    newnote[args[0]]=args[1].split(/\s*[,|\[|\]]\s*/).filter((s)=>s!=="");
                                                }
                                                else{
                                                    //console.log(args)
                                                    newnote[args[0]]=args[1].trim();
                                                }
                                            })

                                        }
                                        
                                    }
                                    else{
                                        newnote.content = file;
                                    }
                                    Carnet.fileList[root+'/'+e]=newnote;
                                    tree.files.push(root+'/'+e);
                                })
                            }
                        }
                    
                })
            })
        }).then(()=>{
            if(incall===0){
                console.log("tree: ",Carnet.tree);
                console.log("dirList: ",Carnet.dirList);
                console.log("fileList: ",Carnet.fileList);
                Carnet.setstatus("Populating tree done");
                Carnet.tree.done = true;
                Carnet.loading = false;
                redraw();
            }
        });
    }
}

Carnet.init();



export default Carnet;
